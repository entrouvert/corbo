#!/bin/sh

set -e

rm -f coverage.xml
rm -f test_results.xml

pip install --upgrade tox
pip install --upgrade pylint pylint-django
tox -r
pip install -r requirements.txt # install dependencies, for better pylint results
test -f pylint.out && cp pylint.out pylint.out.prev
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc corbo/ | tee pylint.out) || /bin/true
test -f pylint.out.prev && (diff pylint.out.prev pylint.out | grep '^[><]' | grep .py) || /bin/true
