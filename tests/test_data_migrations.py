import uuid

import pytest

import django
from django.db import connection
from django.db.migrations.executor import MigrationExecutor

pytestmark = pytest.mark.django_db


def test_subscription_sms_identifier_format_migration():
    if django.VERSION >= (2, 0, 0):
        pytest.skip('NotSupportedError')
    executor = MigrationExecutor(connection)
    app = 'corbo'
    migrate_from = [(app, '0009_auto_20170120_1533')]
    migrate_to = [(app, '0011_auto_20180426_1334')]
    executor.migrate(migrate_from)
    executor.loader.build_graph()
    old_apps = executor.loader.project_state(migrate_from).apps
    Category = old_apps.get_model('corbo', 'Category')
    Subscription = old_apps.get_model('corbo', 'Subscription')
    category = Category.objects.create(name='Science', slug='science')
    for identifier in ('sms:06 10 20 30 40', 'sms:+33 6 10 20 30 40', 'sms:0033610203040',
                       'mailto:john@doe.com', 'sms:+33 (0) 6 10 20 30 40', 'sms:06.10.20:30.40.'):
        Subscription.objects.create(uuid=uuid.uuid4().hex, category=category, identifier=identifier)
    executor.migrate(migrate_to)
    executor.loader.build_graph()
    apps = executor.loader.project_state(migrate_from).apps
    Subscription = apps.get_model('corbo', 'Subscription')
    assert Subscription.objects.count() == 6
    valid_sms_identifier = ['sms:0610203040', 'sms:+33610203040', 'sms:0033610203040']
    for subscription in Subscription.objects.filter(identifier__startswith='sms:'):
        assert subscription.identifier in valid_sms_identifier
