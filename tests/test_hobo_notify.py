import pytest
from copy import deepcopy
import json

from corbo.models import Subscription
from corbo.hobo_agent.management.commands.hobo_notify import Command

import django
from django.core.management import call_command
from django.db.models import Q

if django.VERSION > (1, 8):
    pytest.skip('hobo tests are limited to django 1.8', allow_module_level=True)

NOTIFICATION = {'@type': 'provision',
                'objects': {'@type': 'user',
                            'data': [
                                {'uuid': 'uuid1', 'email': 'foo1@example.net', 'mobile': '0504030201'},
                                {'uuid': 'uuid2', 'email': 'bar1@example.net', 'mobile': '0009080706'},
                            ]},
                'audience': [],
                'full': False
}


def test_notify_provision(tenant, subscriptions):
    command = Command()
    command.process_notification(tenant, NOTIFICATION)
    assert Subscription.objects.count() == len(subscriptions)
    for user in NOTIFICATION['objects']['data']:
        assert Subscription.objects.filter(uuid=user['uuid'], identifier='mailto:%(email)s' % user).exists()
        assert Subscription.objects.filter(uuid=user['uuid'], identifier='sms:%(mobile)s' % user).exists()


def test_notify_other_provision(tenant, subscriptions):
    role_notification = deepcopy(NOTIFICATION)
    role_notification['objects']['@type'] = 'role'
    command = Command()
    command.process_notification(tenant, role_notification)
    assert Subscription.objects.count() == len(subscriptions)
    for subscription in subscriptions:
        assert Subscription.objects.filter(uuid=subscription.uuid, identifier=subscription.identifier)


def test_notify_deprovision(tenant, subscriptions):
    deprovision_notification = deepcopy(NOTIFICATION)
    deprovision_notification['@type'] = 'deprovision'
    command = Command()
    command.process_notification(tenant, deprovision_notification)
    for user in deprovision_notification['objects']['data']:
        assert not Subscription.objects.filter(uuid=user['uuid']).exists()


def test_notify_full(tenant, subscriptions):
    full_notification = deepcopy(NOTIFICATION)
    full_notification['full'] = True
    command = Command()
    command.process_notification(tenant, full_notification)
    # strings empty and null are the same for CharFields
    assert Subscription.objects.filter(Q(uuid='')|Q(uuid__isnull=True)).count() == 8
    assert Subscription.objects.exclude(Q(uuid='')|Q(uuid__isnull=True)).count() == 8
    full_notification['objects']['data'] = []
    command.process_notification(tenant, full_notification)
    # check all remaining subscriptions have empty uuids
    for subscription in Subscription.objects.all():
        assert not subscription.uuid


def test_notify_emails_only(tenant, subscriptions):
    email_notification = deepcopy(NOTIFICATION)
    for user in email_notification['objects']['data']:
        user['mobile'] = None
        user['email'] = 'new-%(email)s' % user
    command = Command()
    command.process_notification(tenant, email_notification)
    for user in email_notification['objects']['data']:
        assert not Subscription.objects.filter(uuid=user['uuid'], identifier__startswith='sms:').exists()


def test_notify_mobiles_only(tenant, subscriptions):
    mobile_notification = deepcopy(NOTIFICATION)
    for user in mobile_notification['objects']['data']:
        user['email'] = None
        user['mobile'] = '0%(mobile)s' % user
    command = Command()
    command.process_notification(tenant, mobile_notification)
    for user in mobile_notification['objects']['data']:
        assert not Subscription.objects.filter(uuid=user['uuid'], identifier__startswith='mailto:').exists()
