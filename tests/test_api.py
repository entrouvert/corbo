import pytest

import urllib
from uuid import uuid4


from django.urls import reverse
from django.utils.http import urlencode
from django.contrib.auth import get_user_model
from django.utils.text import slugify

from corbo.models import Category, Announce, Broadcast, Subscription
from corbo.models import channel_choices

pytestmark = pytest.mark.django_db

CATEGORIES = (u'Alerts', u'News')


@pytest.fixture
def categories():
    categories = []
    for category in CATEGORIES:
        c, created = Category.objects.get_or_create(name=category, slug=slugify(category))
        categories.append(c)
    return categories


@pytest.fixture
def announces():
    announces = []
    for category in Category.objects.all():
        a = Announce.objects.create(category=category, title='By email')
        Broadcast.objects.create(announce=a)
        announces.append(a)
        a = Announce.objects.create(category=category, title='Another one')
        Broadcast.objects.create(announce=a)
        announces.append(a)
    return announces


@pytest.fixture
def user():
    User = get_user_model()
    user = User.objects.create(username='john.doe', first_name=u'John',
                               last_name=u'Doe', email='john.doe@example.net')
    user.set_password('password')
    user.save()
    return user


def test_get_newsletters(app, categories, announces, user):
    resp = app.get(reverse('newsletters'), status=(401, 403))
    app.authorization = ('Basic', ('john.doe', 'password'))
    resp = app.get(reverse('newsletters'))
    data = resp.json
    assert data['data']
    for category in data['data']:
        assert 'id' in category
        assert 'text' in category
        assert category['id'] in [slugify(c) for c in CATEGORIES]
        assert category['text'] in CATEGORIES
        assert 'transports' in category
        assert category['transports'] == [{'id': 'mailto', 'text': 'Email'}, {'id': 'sms', 'text': 'SMS'}]


def test_get_subscriptions(app, categories, announces, user):
    resp = app.get(reverse('subscriptions'), status=(401, 403))
    uuid = str(uuid4())
    resp = app.get(reverse('subscriptions'), params={'uuid': uuid}, status=(401, 403))
    app.authorization = ('Basic', ('john.doe', 'password'))

    for identifier, name in channel_choices:
        for category in categories:
            uri = '%s:%s' % (identifier, '00339988776655')
            Subscription.objects.create(identifier=uri, uuid=uuid, category=category)
        resp = app.get(reverse('subscriptions'), params={'uuid': uuid})
        assert len(resp.json['data']) == 2
        for d in resp.json['data']:
            assert d['id'] in [category.slug for category in categories]
            assert d['text'] in [category.name for category in categories]
            assert identifier in [transport['id'] for transport in d['transports']]


def test_update_subscriptions(app, categories, announces, user):
    uuid = uuid4()
    app.authorization = ('Basic', ('john.doe', 'password'))
    subscriptions_url = '/api/subscriptions/?uuid=%s&email=user@example.com&mobile=0607080900' % uuid
    for category in categories:
        transports = []
        for identifier, name in channel_choices:
            transports.append(identifier)
            subscriptions = [{'id': category.slug,
                              'text': category.name,
                              'transports': transports}]
            resp = app.post_json(subscriptions_url, params=subscriptions)
            resp = app.get(subscriptions_url)
            assert len(resp.json['data']) >= 1
            for cat in resp.json['data']:
                if cat['id'] == category.slug:
                    sub_transports = [c['id'] for c in cat['transports']]
                    assert sub_transports == transports


def test_delete_subscriptions(app, categories, announces, user):
    params = urlencode({'uuid': str(uuid4())})
    subscriptions_url = reverse('subscriptions') + '?' + params
    resp = app.delete(subscriptions_url, status=(401, 403))
    app.authorization = ('Basic', ('john.doe', 'password'))
    resp = app.delete(subscriptions_url)
    if resp.json['data']:
        resp = app.get(subscriptions_url)
        assert resp.json['data'] == []


def test_simple_email_subscription(app, categories, user):
    payload = {'category_id': 'alerts', 'transports': ['mailto']}
    uuid = uuid4()
    url = '/api/subscribe/?uuid=%s&email=john@example.net' % uuid

    # anonymous
    resp = app.post_json(url, params=payload, status=(401, 403))
    assert resp.json['detail'] == 'Authentication credentials were not provided.'

    # authenticated
    app.authorization = ('Basic', ('john.doe', 'password'))
    resp = app.post_json(url, params=payload, status=200)
    assert resp.json['data'] is True

    # with wrong method
    resp = app.get('/api/subscribe/?uuid=%s&email=john@example.net' % uuid, status=405)
    assert resp.json['detail'] == 'Method "GET" not allowed.'

    # right method on right url
    resp = app.get('/api/subscriptions/?uuid=%s' % uuid)

    data = resp.json['data']
    assert len(data) == 1
    assert data[0]['id'] == 'alerts'
    assert data[0]['text'] == 'Alerts'
    assert len(data[0]['transports']) == 1
    transport = data[0]['transports'][0]
    assert transport['id'] == 'mailto'
    assert transport['text'] == 'mailto'

def test_simple_sms_subscription(app, categories, user, phonenumber):
    payload = {'category_id': 'alerts', 'transports': ['sms']}
    uuid = uuid4()
    url_params = urlencode({'uuid': uuid, 'mobile': phonenumber})
    url = '/api/subscribe/?' + url_params
    app.authorization = ('Basic', ('john.doe', 'password'))
    resp = app.post_json(url, params=payload, status=200)
    assert Subscription.objects.get(uuid=uuid).identifier in ['sms:0610203040', 'sms:+33610203040', 'sms:0033610203040']
    resp = app.get('/api/subscriptions/?uuid=%s' % uuid)
    data = resp.json['data']
    assert len(data) == 1
    assert data[0]['id'] == 'alerts'
    assert data[0]['text'] == 'Alerts'
    assert len(data[0]['transports']) == 1
    transport = data[0]['transports'][0]
    assert transport['id'] == 'sms'
    assert transport['text'] == 'sms'
