import pytest
import django_webtest
import copy
import tempfile
import os
import json
import shutil

import django.core.mail.backends.locmem
from django.core.mail.backends.locmem import EmailBackend as DjangoEmailBackend
from django.utils.text import slugify

from corbo.models import Category, Announce, Broadcast, Subscription

CATEGORIES = ('Alerts', 'News')

SUBSCRIBERS = [{'uuid': 'uuid1', 'email': 'foo@example.net', 'mobile': '0102030405'},
               {'uuid': 'uuid2', 'email': 'bar@example.net', 'mobile': '0607080900'},
               {'uuid': '', 'email': 'john@example.net', 'mobile': '0304050607'},
               {'uuid': None, 'email': 'john2@example.net', 'mobile': '0405060708'}]



class MockedEmailBackend(object):

    def create_email_backend(self, *args, **kwargs):
        class EmailBackend(DjangoEmailBackend):
            def __init__(self, *args, **kwargs):
                super(DjangoEmailBackend, self).__init__(*args, **kwargs)

            def send_messages(self, messages):
                new_messages = []
                for message in messages:
                    new_messages.append(copy.deepcopy(message))
                    return super(EmailBackend, self).send_messages(new_messages)

        return EmailBackend

    def __enter__(self):
        import django.core.mail.backends.locmem
        self.locmem_emailbackend = django.core.mail.backends.locmem.EmailBackend
        django.core.mail.backends.locmem.EmailBackend = self.create_email_backend()
        return self

    def __exit__(self, *args, **kwargs):
        django.core.mail.backends.locmem.EmailBackend = self.locmem_emailbackend


@pytest.fixture
def custom_mailoutbox():
    with MockedEmailBackend() as mock:
        yield mock

@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def categories():
    categories = []
    for category in CATEGORIES:
        c, created = Category.objects.get_or_create(name=category, slug=slugify(category))
        categories.append(c)
    return categories


@pytest.fixture
def subscriptions(categories):
    subscriptions = []
    for category in categories:
        for subscriber in SUBSCRIBERS:
            kwargs = {'category': category, 'identifier': 'mailto:%(email)s' % subscriber}
            uuid = subscriber['uuid']
            if uuid is not None:
                kwargs['uuid'] = uuid
            subscriptions.append(Subscription.objects.create(**kwargs))
            kwargs['identifier'] = 'sms:%(mobile)s' % subscriber
            subscriptions.append(Subscription.objects.create(**kwargs))
    return subscriptions


@pytest.fixture
def tenant_base(request, settings):
    base = tempfile.mkdtemp('corbo-tenant-base')
    settings.TENANT_BASE = base

    def fin():
        shutil.rmtree(base)
    request.addfinalizer(fin)
    return base


@pytest.fixture(scope='function')
def tenant(transactional_db, request, tenant_base):
    from hobo.multitenant.models import Tenant
    base = tenant_base

    @pytest.mark.django_db
    def make_tenant(name):
        tenant_dir = os.path.join(base, name)
        os.mkdir(tenant_dir)
        with open(os.path.join(tenant_dir, 'hobo.json'), 'w') as fd:
            json.dump({
                'variables': {
                    'hobo_test_variable': True,
                    'other_variable': 'foo',
                },
                'services': [
                    {'slug': 'test',
                     'service-id': 'corbo',
                     'title': 'Test',
                     'this': True,
                     'secret_key': '12345',
                     'base_url': 'http://%s' % name,
                     'saml-sp-metadata-url': 'http://%s/metadata/' % name,
                     'variables': {
                         'other_variable': 'bar',
                     }
                    },
                    {'slug': 'passerelle',
                     'title': 'Webserivces',
                     'service-id': 'passerelle',
                     'secret_key': 'abcdef',
                     'base_url': 'http://passerelle.example.net',
                     'saml-sp-metadata-url': 'http://passerelle.example.net/metadata/' },
                    ]}, fd)
        return Tenant(domain_url=name,
                      schema_name=name.replace('-', '_').replace('.', '_'))
    return make_tenant('corbo.example.net')


@pytest.fixture(params=['06 10 20 30 40', '+33 6 10 20 30 40',
                '0033610203040', '+33 (0) 6 10 20 30 40', '0033+610203040+'])
def phonenumber(request):
    return request.param
