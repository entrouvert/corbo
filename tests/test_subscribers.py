from __future__ import unicode_literals

import pytest
from webtest import Upload

from django.utils.text import slugify
from django.urls import reverse
from django.contrib.auth import get_user_model

from corbo.models import Category, Subscription

pytestmark = pytest.mark.django_db

CATEGORIES = (u'Alerts',)

CSV_CONTENT = """foo@example.net, Foo,
john.doe@example.net, John Doe,
bar@localhost, Bar,
"""


@pytest.fixture
def categories():
    categories = []
    for category in CATEGORIES:
        c, created = Category.objects.get_or_create(name=category, slug=slugify(category))
        categories.append(c)
    return categories

@pytest.fixture
def admin():
    User = get_user_model()
    admin = User.objects.create_superuser(username='admin',
                password='password', email='admin@example.net')
    return admin

def login(app, username='admin', password='password'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app

def test_subscribe_from_csv(app, admin, categories):
    app = login(app)
    for c in categories:
        page = app.get(reverse('subscriptions-import', kwargs={'slug': c.slug}))
        form = page.form
        form['subscribers'] = Upload('users.csv', CSV_CONTENT.encode('utf-8'))
        res = form.submit()
        assert res.status_code == 302
        assert Subscription.objects.filter(category=c).count() == len(CSV_CONTENT.splitlines())

def test_subscribe_from_csv_with_empty_lines(app, admin, categories):
    app = login(app)
    content = CSV_CONTENT + '\n\n\n'
    for c in categories:
        page = app.get(reverse('subscriptions-import', kwargs={'slug': c.slug}))
        form = page.form
        form['subscribers'] = Upload('users.csv', content.encode('utf-8'))
        res = form.submit()
        assert res.status_code == 302
        assert Subscription.objects.filter(category=c).count() == len(CSV_CONTENT.splitlines())

def test_subscribe_with_invalid_email(app, admin, categories):
    app = login(app)
    content = CSV_CONTENT + '\nwrong, Wrong user,'
    for category in categories:
        page = app.get(reverse('subscriptions-import', kwargs={'slug': category.slug}))
        form = page.form
        form['subscribers'] = Upload('users.csv', content.encode('utf-8'))
        page = form.submit()
        assert page.status_code == 200
        page.mustcontain('Invalid email address at line')
