import os

# Add corbo hobo agent
try:
    import hobo
    INSTALLED_APPS = ('corbo.hobo_agent', 'hobo.agent.common') + INSTALLED_APPS
except ImportError:
    pass


REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = ['rest_framework.authentication.BasicAuthentication']

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.sqlite3'),
        'TEST': {'NAME': 'corbo-test-%s' % os.environ.get("BRANCH_NAME", "").replace('/', '-')[:63],},
    }
}
