# corbo - Announces Manager
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url

from .api_views import NewslettersView, SubscriptionsView, SubscribeView


urlpatterns = [
            url(r'^newsletters/', NewslettersView.as_view(), name='newsletters'),
            url(r'^subscriptions/', SubscriptionsView.as_view(), name='subscriptions'),
            url(r'^subscribe/', SubscribeView.as_view(), name='subscribe'),
]
