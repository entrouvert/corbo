import csv

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_text
from django.utils.text import slugify
from django.core.exceptions import ObjectDoesNotExist
from django.core import validators
from django.core.exceptions import ValidationError
from django.utils import six

from .models import Announce, Category, Broadcast, channel_choices
from . import widgets


DATETIME_OPTIONS = {
        'weekStart': 1,
        'autoclose': True,
}


class DateTimeWidget(widgets.DateTimeWidget):
    def __init__(self, *args, **kwargs):
        super(DateTimeWidget, self).__init__(*args, options=DATETIME_OPTIONS, **kwargs)


class AnnounceForm(forms.ModelForm):

    class Meta:
        model = Announce
        exclude = ('identifier',)
        widgets = {
            'publication_time': DateTimeWidget(),
            'expiration_time': DateTimeWidget(),
            'category': forms.HiddenInput()
        }

    def save(self, *args, **kwargs):
        instance = super(AnnounceForm, self).save(*args, **kwargs)
        if instance:
            Broadcast.objects.get_or_create(announce=instance)
        return instance


class CategoryForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'rss_feed_url')
        model = Category

    def save(self, commit=True):
        slug = slugify(self.instance.name)
        base_slug = slug
        if not self.instance.slug:
            i = 1
            while True:
                try:
                    c = Category.objects.get(slug=slug)
                except ObjectDoesNotExist:
                    break
                i += 1
                slug = '%s-%s' % (base_slug, i)
            self.instance.slug = slug
        return super(CategoryForm, self).save(commit=commit)


class SubscriptionsImportForm(forms.Form):
    subscribers = forms.FileField(label=_('Subscribers'),
                    help_text=_('utf-8 encoded, comma separated file with email addresses on first column'))

    def clean_subscribers(self, *args, **kwargs):
        subscribers = []
        if six.PY3:
            self.cleaned_data['subscribers'] = force_text(self.cleaned_data['subscribers'].read()).splitlines()
        reader = csv.reader(self.cleaned_data['subscribers'])
        for idx, row in enumerate(reader, 1):
            if not row or not row[0]:
                continue
            try:
                validators.validate_email(row[0])
            except ValidationError:
                raise ValidationError(_('Invalid email address at line %d' % idx))
            subscribers.append(row[0])
        return subscribers


class SendTestEmailForm(forms.Form):
    email = forms.EmailField(label=_('Email'))


class SendTestSMSForm(forms.Form):
    mobile = forms.CharField(label=_('Mobile number'))
