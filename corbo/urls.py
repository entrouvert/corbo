from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

from django.views.decorators.cache import never_cache
from django.contrib.admin.views.decorators import staff_member_required

from .urls_utils import decorated_includes, manager_required
from .views import homepage, atom, unsubscribe, unsubscription_done, LoginView, LogoutView

from .manage_urls import urlpatterns as manage_urls
from .api_urls import urlpatterns as api_urls

import ckeditor.views as ckeditor_views


urlpatterns = [
    url(r'^$', homepage, name='home'),
    url(r'^atom$', atom, name='atom'),
    url(r'^manage/', decorated_includes(manager_required,
                    include(manage_urls))),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urls)),
    url(r'^unsubscribe/done/$', unsubscription_done,
        name='unsubscription_done'),
    url(r'^unsubscribe/(?P<unsubscription_token>[\w:-]+)$', unsubscribe,
        name='unsubscribe'),
    url(r'^logout/$',LogoutView.as_view(), name='auth_logout'),
    url(r'^login/$', LoginView.as_view(), name='auth_login'),
    url(r'^ckeditor/upload/', staff_member_required(ckeditor_views.upload),
        name='ckeditor_upload'),
    url(r'^ckeditor/browse/', never_cache(staff_member_required(ckeditor_views.browse)),
         name='ckeditor_browse'),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(url(r'^accounts/mellon/', include('mellon.urls')))

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
