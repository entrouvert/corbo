# corbo - Announces Manager
# Copyright (C) 2017 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.models import Q

from hobo.agent.common.management.commands import hobo_notify

from corbo.models import Subscription

class Command(hobo_notify.Command):

    def process_notification(self, tenant, notification):
        super(Command, self).process_notification(tenant, notification)
        object_type = notification['objects']['@type']
        if object_type != 'user':
            return
        users = notification['objects']['data']
        action = notification['@type']
        if notification.get('full'):
            uuids = [user['uuid'] for user in users]
            Subscription.objects.exclude(Q(uuid__in=uuids)|Q(uuid__isnull=True)|Q(uuid='')).delete()

        for user in users:
            for subscription in Subscription.objects.filter(uuid=user['uuid']):
                if action == 'provision':
                    if subscription.identifier.startswith('mailto:'):
                        if user.get('email'):
                            subscription.identifier = 'mailto:%s' % user['email']
                        else:
                            subscription.delete()
                            continue
                    elif subscription.identifier.startswith('sms:'):
                        if user.get('mobile'):
                            subscription.identifier = 'sms:%s' % user['mobile']
                        else:
                            subscription.delete()
                            continue
                    subscription.save()
                elif action == 'deprovision':
                    subscription.delete()
