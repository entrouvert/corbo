# corbo - Announces Manager
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from collections import defaultdict

from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.db import transaction
from django.utils.encoding import force_text

from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Category, Subscription, channel_choices


class NewslettersView(APIView):

    def get(self, request):
        newsletters = []
        transports = [{'id': identifier, 'text': name} for identifier, name in channel_choices]
        for c in Category.objects.all():
            newsletter = {'id': c.slug, 'text': c.name,
                          'transports': transports}
            newsletters.append(newsletter)
        return Response({'data': newsletters})


class SubscriptionsView(APIView):

    def get_subscriptions(self, uuid):
        subscriptions = defaultdict(dict)
        for s in Subscription.objects.filter(uuid=uuid):
            cat_id = s.category.pk
            subscriptions[cat_id]['id'] = s.category.slug
            subscriptions[cat_id]['text'] = s.category.name
            transport_id, transport_name = s.identifier.split(':')
            transport = {'id': transport_id,
                         'text': transport_id}
            if 'transports' in subscriptions[cat_id]:
                subscriptions[cat_id]['transports'].append(transport)
            else:
                subscriptions[cat_id]['transports'] = [transport]

        return subscriptions.values()

    @transaction.atomic
    def update_subscriptions(self, category_slug, transports, uuid, email=None, mobile=None):
        try:
            cat = Category.objects.get(slug=category_slug)
        except Category.DoesNotExist:
            return

        subcriptions = cat.subscription_set

        if 'mailto' in transports:
            if email:
                subcriptions.get_or_create(identifier='mailto:%s' % email, uuid=uuid)
        else:
            subcriptions.filter(uuid=uuid, identifier__startswith='mailto:').delete()

        if 'sms' in transports:
            if mobile:
                subcriptions.get_or_create(identifier='sms:%s' % mobile, uuid=uuid)
        else:
            subcriptions.filter(uuid=uuid, identifier__startswith='sms:').delete()

    def get(self, request):
        uuid = request.GET.get('uuid')
        if not uuid:
            raise PermissionDenied('Uuid parameter required')
        return Response({'data': self.get_subscriptions( uuid)})

    def post(self, request):
        uuid = request.GET.get('uuid')
        email = request.GET.get('email')
        mobile = request.GET.get('mobile')
        if not uuid:
            raise PermissionDenied('Uuid parameter required')
        data = json.loads(force_text(request.body))
        for subscription in data:
            self.update_subscriptions(subscription['id'], subscription['transports'],
                                      uuid, email, mobile)
        return Response({'data': True})

    def delete(self, request):
        uuid = request.GET.get('uuid')
        if not uuid:
            raise PermissionDenied('Uuid parameter required')
        for subscription in self.get_subscriptions(uuid):
            self.update_subscriptions(subscription['id'], [], uuid)
        return Response({'data': True})


class SubscribeView(SubscriptionsView):
    http_method_names = ['post']

    def post(self, request):
        email = request.GET.get('email')
        mobile = request.GET.get('mobile')
        uuid = request.GET.get('uuid')
        if not uuid:
            raise PermissionDenied('Uuid parameter required')
        data = json.loads(force_text(request.body))

        self.update_subscriptions(data['category_id'], data['transports'], uuid,
                                  email, mobile)

        return Response({'data': True})
