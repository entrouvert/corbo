from django.conf.urls import url

from .views import add_announce, edit_announce, delete_announce, \
    add_category, edit_category, view_category, delete_category, manage, \
    subscriptions_import, view_announce, email_announce, sms_announce, \
    menu_json, subscription_delete, subscription_search

urlpatterns = [
            url(r'^$', manage, name='manage'),
            url(r'^category/(?P<slug>[\w-]+)/announce/$', add_announce,
                name='add_announce'),
            url(r'^announce/edit/(?P<pk>\d+)$', edit_announce,
                name='edit_announce'),
            url(r'^announce/delete/(?P<pk>\d+)$', delete_announce,
                name='delete_announce'),
            url(r'^announce/email/(?P<pk>\d+)/$', email_announce,
                name='email_announce'),
            url(r'^announce/sms/(?P<pk>\d+)/$', sms_announce,
                name='sms_announce'),
            url(r'^category/(?P<slug>[\w-]+)/$', view_category,
                name='view_category'),
            url(r'^announce/(?P<pk>\d+)/$', view_announce,
                name='view_announce'),
            url(r'^category/add$', add_category,
                name='add_category'),
            url(r'^category/edit/(?P<slug>[\w-]+)$', edit_category,
                name='edit_category'),
            url(r'^category/delete/(?P<slug>[\w-]+)$', delete_category,
                name='delete_category'),
            url(r'^category/(?P<slug>[\w-]+)/import-subscriptions/$', subscriptions_import,
                name='subscriptions-import'),
            url(r'^subscription/delete/(?P<pk>[\w-]+)/$', subscription_delete,
                name='subscription-delete'),
            url(r'^subscriptions/search$', subscription_search,
                name='subscription-search'),
            url(r'^menu.json$', menu_json),
]
