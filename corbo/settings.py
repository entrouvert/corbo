"""
Django settings for corbo project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from django.conf import global_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$%7m&rq1-&$c77a1s^_$=xgiqez-%_x3^&5=*^-4(6si2zu59z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'corbo',
    'ckeditor',
    'gadjo',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'corbo.urls'

WSGI_APPLICATION = 'corbo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATICFILES_FINDERS = tuple(global_settings.STATICFILES_FINDERS) + ('gadjo.finders.XStaticFinder',)

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = 'static'

CKEDITOR_UPLOAD_PATH = 'ckeditor/uploads'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'allowedContent': True,
        'removePlugins': 'stylesheetparser',
        'toolbar_Own': [['Source', 'Format', '-', 'Bold', 'Italic'],
                        ['NumberedList', 'BulletedList'],
                        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                        ['Link', 'Unlink'],
                        ['Image',],
                        ['RemoveFormat',],
                        ['Maximize']],
        'toolbar': 'Own',
        'resize_enabled': False,
    },
}

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

ANNOUNCES_PER_PAGE = 20
CATEGORIES_PER_PAGE = 15

RSS_TITLE = 'Announces'
RSS_DESCRIPTION = ''
RSS_LINK = ''
RSS_LINK_TEMPLATE = '/#announce{0}'
RSS_ITEMS_LIMIT = 15

# Authentication settings
try:
    import mellon
except ImportError:
    mellon = None

if mellon is not None:
    INSTALLED_APPS += ('mellon',)
    AUTHENTICATION_BACKENDS = (
        'mellon.backends.SAMLBackend',
        'django.contrib.auth.backends.ModelBackend',
    )

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []

if 'REST_FRAMEWORK' not in globals():
    REST_FRAMEWORK = {}

REST_FRAMEWORK['DEFAULT_PERMISSION_CLASSES'] = (
    'rest_framework.permissions.IsAuthenticated',
)

# default site
SITE_BASE_URL = 'http://localhost'

# default SMS Gateway
SMS_GATEWAY_URL = None

# sms expeditor
SMS_EXPEDITOR = 'Corbo'

# proxies argument passed to all python-request methods
# (see http://docs.python-requests.org/en/master/user/advanced/#proxies)
REQUESTS_PROXIES = None


CORBO_PHONE_SEARCH_DIGITS = 9


local_settings_file = os.environ.get('CORBO_SETTINGS_FILE',
        os.path.join(os.path.dirname(__file__), 'local_settings.py'))
if os.path.exists(local_settings_file):
    exec(open(local_settings_file).read())
