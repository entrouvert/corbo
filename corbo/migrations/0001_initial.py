# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Announce',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='maximum 256 characters', max_length=256, verbose_name='title')),
                ('text', models.TextField(verbose_name='text')),
                ('publication_time', models.DateTimeField(default=django.utils.timezone.now, null=True, verbose_name='publication time', blank=True)),
                ('expiration_time', models.DateTimeField(null=True, verbose_name='expiration time', blank=True)),
                ('ctime', models.DateTimeField(auto_now_add=True, verbose_name='creation time')),
                ('mtime', models.DateTimeField(auto_now=True, verbose_name='modification time')),
            ],
            options={
                'ordering': ('-mtime',),
                'verbose_name': 'announce',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('ctime', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.ForeignKey(verbose_name='category',
                    to='corbo.Category', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(verbose_name='user', blank=True,
                    to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubscriptionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(help_text='ex.: email, mobile phone number, jabber id', max_length=128, verbose_name='identifier', blank=True)),
                ('subscription', models.ForeignKey(to='corbo.Subscription', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('user', 'category')]),
        ),
        migrations.AddField(
            model_name='announce',
            name='category',
            field=models.ForeignKey(verbose_name='category',
                to='corbo.Category', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
