# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from corbo.utils import format_phonenumber


def format_sms_identifiers(apps, schema_editor):
    Subscription = apps.get_model('corbo', 'Subscription')
    uri = 'sms:'
    for subscription in Subscription.objects.filter(identifier__startswith=uri):
        _, phonenumber = subscription.identifier.split(':', 1)
        subscription.identifier = uri + format_phonenumber(phonenumber)
        subscription.save()


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0010_broadcast_delivery_count'),
    ]

    operations = [
        migrations.RunPython(format_sms_identifiers, format_sms_identifiers),
    ]
