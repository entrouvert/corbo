# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0003_auto_20160427_1342'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriptiontype',
            name='subscription',
        ),
        migrations.DeleteModel(
            name='SubscriptionType',
        ),
        migrations.AddField(
            model_name='subscription',
            name='identifier',
            field=models.CharField(help_text='ex.: mailto, ...', max_length=128, verbose_name='identifier', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='subscription',
            name='uuid',
            field=models.CharField(max_length=128, verbose_name='User identifier', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='subscription',
            name='category',
            field=models.ForeignKey(verbose_name='Category', to='corbo.Category', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='broadcast',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='broadcast',
            name='channel',
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('category', 'identifier', 'uuid')]),
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='user',
        ),
    ]
