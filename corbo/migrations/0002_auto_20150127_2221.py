# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Broadcast',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('channel', models.CharField(max_length=32, verbose_name='channel', choices=[(b'sms', 'SMS'), (b'email', 'Email')])),
                ('time', models.DateTimeField(auto_now_add=True, verbose_name='sent time')),
                ('result', models.TextField(verbose_name='result', blank=True)),
                ('announce', models.ForeignKey(verbose_name='announce',
                    to='corbo.Announce', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('-time',),
                'verbose_name': 'sent',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='announce',
            name='expiration_time',
            field=models.DateTimeField(null=True, verbose_name='Expiration date', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='announce',
            name='publication_time',
            field=models.DateTimeField(null=True, verbose_name='Publication date', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='announce',
            name='text',
            field=models.TextField(verbose_name='Content'),
            preserve_default=True,
        ),
    ]
