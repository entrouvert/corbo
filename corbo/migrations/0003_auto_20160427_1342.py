# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0002_auto_20150127_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='announce',
            name='text',
            field=ckeditor.fields.RichTextField(verbose_name='Content'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='broadcast',
            unique_together=set([('announce', 'channel')]),
        ),
    ]
