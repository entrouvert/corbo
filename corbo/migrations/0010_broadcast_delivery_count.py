# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models, connection

def migrate_delivery_count(apps, schema_editor):
    Broadcast = apps.get_model('corbo', 'Broadcast')
    for broadcast in Broadcast.objects.all():
        try:
            broadcast.delivery_count = int(broadcast.result)
        except ValueError:
            broadcast.delivery_count = 0
        broadcast.save()

def migrate_result(apps, schema_editor):
    Broadcast = apps.get_model('corbo', 'Broadcast')
    for broadcast in Broadcast.objects.all():
        broadcast.result = str(broadcast.delivery_count)
        broadcast.save()

class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0009_auto_20170120_1533'),
    ]

    pg_forward_constraint = [migrations.RunSQL('SET CONSTRAINTS ALL IMMEDIATE',
                reverse_sql=migrations.RunSQL.noop),]

    pg_backward_constraint = [migrations.RunSQL(migrations.RunSQL.noop,
                reverse_sql='SET CONSTRAINTS ALL IMMEDIATE'),]

    default_operations = [
        migrations.AddField(
            model_name='broadcast',
            name='delivery_count',
            field=models.IntegerField(default=0, verbose_name='Delivery count'),
        ),
        migrations.RunPython(migrate_delivery_count, migrate_result),
        migrations.RemoveField(
            model_name='broadcast',
            name='result',
        ),
    ]
    operations = pg_forward_constraint + default_operations + pg_backward_constraint if connection.vendor == 'postgresql' else default_operations
