# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0006_auto_20160928_0833'),
    ]

    operations = [
        migrations.AddField(
            model_name='announce',
            name='identifier',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='category',
            name='rss_feed_url',
            field=models.URLField(help_text='if defined, announces will be automatically created from rss items', null=True, verbose_name='Feed URL', blank=True),
        ),
    ]
