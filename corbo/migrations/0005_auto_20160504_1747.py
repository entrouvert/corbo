# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0004_auto_20160504_1744'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='broadcast',
            options={'ordering': ('-deliver_time',), 'verbose_name': 'sent'},
        ),
        migrations.RemoveField(
            model_name='broadcast',
            name='time',
        ),
        migrations.AddField(
            model_name='broadcast',
            name='deliver_time',
            field=models.DateTimeField(null=True, verbose_name='Deliver time'),
            preserve_default=True,
        ),
    ]
