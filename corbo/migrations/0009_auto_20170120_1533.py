# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.text import slugify


class Migration(migrations.Migration):

    dependencies = [
        ('corbo', '0008_category_slug'),
    ]

    def make_slug_unique(apps, schema_editor):
        Category = apps.get_model('corbo', 'Category')
        for category in Category.objects.all():
            category.slug = slugify(category.name)
            category.save()

    operations = [
        migrations.RunPython(make_slug_unique),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug'),
        ),
    ]
