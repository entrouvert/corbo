PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

*/5 * * * * corbo test -x /usr/bin/corbo-manage && corbo-manage tenant_command send_announces --all-tenants
0 * * * * corbo test -x /usr/bin/corbo-manage && corbo-manage tenant_command sync_external_feeds --all-tenants
