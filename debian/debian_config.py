# This file is sourced by "exec" from corbo.settings

import os

PROJECT_NAME = 'corbo'

#
# hobotization (multitenant)
#
exec(open('/usr/lib/hobo/debian_config_common.py').read())

INSTALLED_APPS = ('corbo.hobo_agent', ) + INSTALLED_APPS

#
# local settings
#
exec(open(os.path.join(ETC_DIR, 'settings.py')).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
